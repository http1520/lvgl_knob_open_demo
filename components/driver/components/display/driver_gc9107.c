
/*
@driver_gc9107.c
@author: LH 
Creation Date：2024/11/04
*/
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "driver_spi.h"
#include "driver_gpio.h"
#include "driver_dma.h"
#include "driver_gc9107.h"

#include "sys_utils.h"

#define DEVELOP_FR8008GP_BOARD 1

#ifdef DEVELOP_FR8008GP_BOARD

#define LCD_CS_PORT			GPIO_B
#define LCD_CS_PIN			GPIO_PIN_7

#define LCD_CLK_PORT		GPIO_B
#define LCD_CLK_PIN			GPIO_PIN_0

#define LCD_SDA_PORT		GPIO_B //mosi
#define LCD_SDA_PIN			GPIO_PIN_2

#define LCD_DC_PORT			GPIO_B
#define LCD_DC_PIN			GPIO_PIN_3

#define LCD_RESET_PORT		GPIO_B
#define LCD_RESET_PIN		GPIO_PIN_4

#define LCD_BACKLIGHT_PORT	GPIO_E
#define LCD_BACKLIGHT_PIN	GPIO_PIN_1

#define LCD_RELEASE_CS()            gpio_write_pin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET)
#define LCD_SET_CS()                gpio_write_pin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_CLEAR)

#define LCD_RELEASE_DC()            gpio_write_pin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET)
#define LCD_SET_DC()                gpio_write_pin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_CLEAR)

#define LCD_RELEASE_RESET()         gpio_write_pin(LCD_RESET_PORT, LCD_RESET_PIN, GPIO_PIN_SET)
#define LCD_SET_RESET()             gpio_write_pin(LCD_RESET_PORT, LCD_RESET_PIN, GPIO_PIN_CLEAR)

#define LCD_ENABLE_BACKLIGHT()      gpio_write_pin(LCD_BACKLIGHT_PORT, LCD_BACKLIGHT_PIN, GPIO_PIN_SET)
#define LCD_DISABLE_BACKLIGHT()     gpio_write_pin(LCD_BACKLIGHT_PORT, LCD_BACKLIGHT_PIN, GPIO_PIN_CLEAR)


#define LCD_W 128
#define LCD_H 128
#endif

void gc9107_display_wait_transfer_done(void);
static DMA_LLI_InitTypeDef Link_Channel[30];
static DMA_HandleTypeDef gc9107_DMA_Channel;
static dma_LinkParameter_t LinkParameter;
static volatile bool dma_transfer_done = true;

SPI_HandleTypeDef gc9107_SPI_Handle;

static void (*dma_trans_done_callback)(void) = NULL;
 

 

static void gc9107_init_io(void)
{
	 __SYSTEM_GPIO_CLK_ENABLE();
    GPIO_InitTypeDef GPIO_Handle;
   
	// backlight
    GPIO_Handle.Pin       = LCD_BACKLIGHT_PIN;
    GPIO_Handle.Mode      = GPIO_MODE_OUTPUT_PP;
    gpio_init(LCD_BACKLIGHT_PORT, &GPIO_Handle);
 
	 // reset  DC   CS 
    GPIO_Handle.Pin       = LCD_RESET_PORT|LCD_DC_PIN|LCD_CS_PIN;
	GPIO_Handle.Alternate = GPIO_FUNCTION_0;
    GPIO_Handle.Mode      = GPIO_MODE_OUTPUT_PP;
    gpio_init(GPIO_B, &GPIO_Handle);


    GPIO_Handle.Pin       = GPIO_PIN_0|GPIO_PIN_2;//clk mosi
    GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
    GPIO_Handle.Pull      = GPIO_PULLDOWN;//GPIO_PULLDOWN;   GPIO_PULLUP
    GPIO_Handle.Alternate = GPIO_FUNCTION_2;
    gpio_init(GPIO_B, &GPIO_Handle);
 
    LCD_RELEASE_CS();
    LCD_ENABLE_BACKLIGHT();
 
 
	
}

static void gc9107_init_spi(void)
{
    __SYSTEM_SPI0_MASTER_CLK_SELECT_96M();
    __SYSTEM_SPI0_MASTER_CLK_ENABLE();
    
    gc9107_SPI_Handle.SPIx                       = SPIM0;
    gc9107_SPI_Handle.Init.Work_Mode             = SPI_WORK_MODE_0;
    gc9107_SPI_Handle.Init.Frame_Size            = SPI_FRAME_SIZE_8BIT;
    gc9107_SPI_Handle.Init.BaudRate_Prescaler    = 2;
    gc9107_SPI_Handle.Init.TxFIFOEmpty_Threshold = 15;
    gc9107_SPI_Handle.Init.RxFIFOFull_Threshold  = 0;

    spi_master_init(&gc9107_SPI_Handle);
}

static void gc9107_init_dma(void)
{    
    __SYSTEM_DMA_CLK_ENABLE();
    __DMA_REQ_ID_SPI0_MASTER_TX(1);
    
    gc9107_DMA_Channel.Channel = DMA_Channel0;
    gc9107_DMA_Channel.Init.Data_Flow        = DMA_M2P_DMAC;
    gc9107_DMA_Channel.Init.Request_ID       = 1;
    gc9107_DMA_Channel.Init.Source_Inc       = DMA_ADDR_INC_INC;
    gc9107_DMA_Channel.Init.Desination_Inc   = DMA_ADDR_INC_NO_CHANGE;
    gc9107_DMA_Channel.Init.Source_Width     = DMA_TRANSFER_WIDTH_32;
    gc9107_DMA_Channel.Init.Desination_Width = DMA_TRANSFER_WIDTH_16;
    dma_init(&gc9107_DMA_Channel);

    NVIC_EnableIRQ(DMA_IRQn);
}

static void gc9107_write_cmd(uint8_t cmd)
{
    uint8_t spi_data[4];

    spi_data[0]=cmd;
	LCD_SET_DC();
    LCD_SET_CS();
    spi_master_transmit_X1(&gc9107_SPI_Handle, (uint16_t *)spi_data, 1);
    LCD_RELEASE_CS();

}

static void gc9107_write_data(uint8_t data)
{
    uint8_t spi_data[4];

    spi_data[0]=data;
	LCD_RELEASE_DC();
    LCD_SET_CS();
    spi_master_transmit_X1(&gc9107_SPI_Handle, (uint16_t *)spi_data, 1);
    LCD_RELEASE_CS();
}

static void gc9107_write_16bit_data(uint16_t data)
{
    uint8_t spi_data[4];

    spi_data[0]=data>>8;
	spi_data[1]=data;
	LCD_RELEASE_DC();
    LCD_SET_CS();
    spi_master_transmit_X1(&gc9107_SPI_Handle, (uint16_t *)spi_data, 2);
    LCD_RELEASE_CS();
}


//static void send_data_disp(uint8_t *pdata,uint32_t len)
//{
//	
//	  LCD_RELEASE_DC();
//    LCD_SET_CS();
//    spi_master_transmit_X1(&gc9107_SPI_Handle, (uint16_t *)pdata, len);
//	  LCD_RELEASE_CS();
//}


 // 函数定义：将16位无符号整数的字节顺序进行大小端转换
//static uint16_t swap_endian(uint16_t value) {
//    return ((value & 0xFF) << 8) | ((value & 0xFF00) >> 8);
//}

#if 0
static int8_t display_page = 0;

static uint16_t *PSRAM_LCD_FRAME_BUFFER_ORIGIN = (uint16_t *)(0x22000000+0x200000);
static uint16_t *PSRAM_LCD_FRAME_BUFFER_LEFT =  (uint16_t *)(0x22000000 + 0x50000+0x200000);
static uint16_t *PSRAM_LCD_FRAME_BUFFER_RIGHT = (uint16_t *)(0x22000000 + 0x100000+0x200000);
static uint16_t *psram = NULL;

static uint16_t color[4]={0xFFFF,0xF800,0x07e0,0x001F};//rgb 565

static void psram_frame_buffer_init(void)
{
	memset(PSRAM_LCD_FRAME_BUFFER_ORIGIN, 0, LCD_W*LCD_H*2);
	memset(PSRAM_LCD_FRAME_BUFFER_LEFT, 0, LCD_W*LCD_H*2);
	memset(PSRAM_LCD_FRAME_BUFFER_RIGHT, 0, LCD_W*LCD_H*2);
	
	for(uint32_t i = 0; i < LCD_W*LCD_H; i++)PSRAM_LCD_FRAME_BUFFER_ORIGIN[i]   = swap_endian(color[1]);//clear
	for(uint32_t i = 0; i < LCD_W*LCD_H; i++)PSRAM_LCD_FRAME_BUFFER_LEFT[i]   = swap_endian(color[2]);//clear
	for(uint32_t i = 0; i < LCD_W*LCD_H; i++)PSRAM_LCD_FRAME_BUFFER_RIGHT[i]   = swap_endian(color[3]);//clear
	uint16_t color_temp=0;
	
 
}

static uint32_t change_count=0;

static void user_display(void)
{
	 
	 if(change_count++ >= 100) 
	{
		change_count = 0;
		
		 switch(display_page)
		{
			case 0://
				psram = PSRAM_LCD_FRAME_BUFFER_ORIGIN;
			
			break;
			case 1://
				psram = PSRAM_LCD_FRAME_BUFFER_LEFT;

			break;
			case 2://
				//for(uint32_t i = 0; i < LCD_W*LCD_H*2; i++)PSRAM_LCD_FRAME_BUFFER_RIGHT[i]   = color[1];//clear
				psram = PSRAM_LCD_FRAME_BUFFER_RIGHT;
				 
			break;
		}
		display_page++;
		if(display_page == 3)display_page=0;
		uint32_t time = system_get_curr_time();		
		 __SPI_DATA_FRAME_SIZE(gc9107_SPI_Handle.SPIx, SPI_FRAME_SIZE_8BIT);
		 gc9107_set_window(0, LCD_W-1,0, LCD_H-1);	 
		 gc9107_display((LCD_W*LCD_H),(void*)psram,NULL);
		 gc9107_display_wait_transfer_done();
		//send_data_disp((uint8_t*)psram,LCD_W*LCD_H*2);	
//		 __SPI_DATA_FRAME_SIZE(gc9107_SPI_Handle.SPIx, SPI_FRAME_SIZE_16BIT);
		co_printf("time:%d\r\n",(system_get_curr_time()-time));
		co_printf("display_page%d\r\n",display_page);
	}

		
 
}
#endif


/******************************************************************************
      函数说明：LCD清屏函数
      入口数据：无
      返回值：  无
******************************************************************************/
void gc9107_Clear(uint16_t Color)
{
	uint16_t i,j;  	
	gc9107_set_window(0,LCD_W-1,0,LCD_H-1);
	for(i=0;i<LCD_W;i++)
	{
		for (j=0;j<LCD_H;j++)
	   	{
        	gc9107_write_16bit_data(Color);	 			 
	    }
 
	 }
}

 
void gc9107_init(void)
{

    gc9107_init_io();
    gc9107_init_spi();
    gc9107_init_dma();
    
	
	
	LCD_RELEASE_RESET();
	co_delay_100us(10);
	LCD_SET_RESET();
	co_delay_100us(500);
	LCD_RELEASE_RESET();
	co_delay_100us(1200);

	#if 0
	gc9107_write_cmd(0x11); 			//Sleep Out
	co_delay_100us(1200);                //ms
	gc9107_write_cmd(0x29);
	co_delay_100us(1200);       
	
	
   //GC9107F+BOE0.96  VCI=2.8V

	gc9107_write_cmd(0xFE);
	gc9107_write_cmd(0xFE);				
	gc9107_write_cmd(0xEF);
		
		
	gc9107_write_cmd(0x21); 	
		
	gc9107_write_cmd(0xB0);			
	gc9107_write_data(0xC0); 
		
	gc9107_write_cmd(0xB2);		
	gc9107_write_data(0x24); 
			
	gc9107_write_cmd(0xB3);		
	gc9107_write_data(0x03);
		
	gc9107_write_cmd(0xB7);		
	gc9107_write_data(0x01);  

	gc9107_write_cmd(0xB6);			
	gc9107_write_data(0x19); 
		
	gc9107_write_cmd(0xAC);
	gc9107_write_data(0xDB);
	gc9107_write_cmd(0xAB);
	gc9107_write_data(0x0f);
	
	gc9107_write_cmd(0x3A);		
	gc9107_write_data(0x05); 
			
	gc9107_write_cmd(0xB4);	
	gc9107_write_data(0x04);

	gc9107_write_cmd(0xA8);	
	gc9107_write_data(0x0C);

	gc9107_write_cmd(0xb8);
	gc9107_write_data(0x08);
			
	gc9107_write_cmd(0xED);	
	gc9107_write_data(0x03);
			
	gc9107_write_cmd(0xea);
	gc9107_write_data(0x9f); 

	gc9107_write_cmd(0xc6);
	gc9107_write_data(0x2a); 

	gc9107_write_cmd(0xc7);	
	gc9107_write_data(0x10); 

	gc9107_write_cmd(0xF0);	
	gc9107_write_data(0x09);
	gc9107_write_data(0x35);
	gc9107_write_data(0x2a);
	gc9107_write_data(0x4a);
	gc9107_write_data(0xA9);
	gc9107_write_data(0x39);
	gc9107_write_data(0x35);
	gc9107_write_data(0x60);
	gc9107_write_data(0x00);
	gc9107_write_data(0x14);
	gc9107_write_data(0x0a);
	gc9107_write_data(0x16);
	gc9107_write_data(0x10);
	gc9107_write_data(0x1F);

	gc9107_write_cmd(0xF1);	
	gc9107_write_data(0x13);
	gc9107_write_data(0x24);
	gc9107_write_data(0x55);
	gc9107_write_data(0x3c);
	gc9107_write_data(0xc6);
	gc9107_write_data(0x16);
	gc9107_write_data(0x3f);
	gc9107_write_data(0x60);
	gc9107_write_data(0x08);
	gc9107_write_data(0x06);
	gc9107_write_data(0x0d);
	gc9107_write_data(0x1f);
	gc9107_write_data(0x1c);
	gc9107_write_data(0x10);
 
#else   
	gc9107_write_cmd(0x11); 			//Sleep Out
	co_delay_100us(1200);                //ms
	gc9107_write_cmd(0x29);
	co_delay_100us(1200);  

	gc9107_write_cmd(0xFE);
	gc9107_write_cmd(0xFE);				
	gc9107_write_cmd(0xEF);

	gc9107_write_cmd(0xB0);		
	gc9107_write_data(0xC0); 
	gc9107_write_cmd(0xB2);			
	gc9107_write_data(0x2F); 
	gc9107_write_cmd(0xB3);		
	gc9107_write_data(0x03);
	gc9107_write_cmd(0xB6);		
	gc9107_write_data(0x19); 
	gc9107_write_cmd(0xB7);		
	gc9107_write_data(0x01);  
		
	gc9107_write_cmd(0xAC);
	gc9107_write_data(0xCB);
	gc9107_write_cmd(0xAB); 
	gc9107_write_data(0x0e);
			
	gc9107_write_cmd(0xB4);	
	gc9107_write_data(0x04);
		
	gc9107_write_cmd(0xA8);
	gc9107_write_data(0x19);

	gc9107_write_cmd(0x36);	
	gc9107_write_data(0x08);

	gc9107_write_cmd(0x3A);	
	gc9107_write_data(0x05);	
	//gc9107_write_data(0x05); 
	 
	gc9107_write_cmd(0xb8);
	gc9107_write_data(0x08);
	 
	gc9107_write_cmd(0xE8);
	gc9107_write_data(0x24);

	gc9107_write_cmd(0xE9);
	gc9107_write_data(0x48);

	gc9107_write_cmd(0xea);	
	gc9107_write_data(0x22);

				
	gc9107_write_cmd(0xC6);
	gc9107_write_data(0x30);
	gc9107_write_cmd(0xC7);
	gc9107_write_data(0x18);

	gc9107_write_cmd(0xF0);
	gc9107_write_data(0x1F);
	gc9107_write_data(0x28);
	gc9107_write_data(0x04);
	gc9107_write_data(0x3E);
	gc9107_write_data(0x2A);
	gc9107_write_data(0x2E);
	gc9107_write_data(0x20);
	gc9107_write_data(0x00);
	gc9107_write_data(0x0C);
	gc9107_write_data(0x06);
	gc9107_write_data(0x00);
	gc9107_write_data(0x1C);
	gc9107_write_data(0x1F);
	gc9107_write_data(0x0f);

	gc9107_write_cmd(0xF1); 
	gc9107_write_data(0X00);
	gc9107_write_data(0X2D);
	gc9107_write_data(0X2F);
	gc9107_write_data(0X3C);
	gc9107_write_data(0X6F);
	gc9107_write_data(0X1C);
	gc9107_write_data(0X0B);
	gc9107_write_data(0X00);
	gc9107_write_data(0X00);
	gc9107_write_data(0X00);
	gc9107_write_data(0X07);
	gc9107_write_data(0X0D);
	gc9107_write_data(0X11);
	gc9107_write_data(0X0f);

	 gc9107_write_cmd(0x20);
	co_delay_100us(1200);  
	 gc9107_write_cmd(0x11);
	co_delay_100us(1200);  
	gc9107_write_cmd(0x29);
	co_delay_100us(1200);  
#endif
//    gc9107_Clear(0xf800);
//	co_delay_100us(10000);
//	 gc9107_Clear(0x07e0);
//	co_delay_100us(10000);
//	 gc9107_Clear(0x001f);
//	co_delay_100us(10000);
//	gc9107_Clear(0xffff);
//	co_delay_100us(10000);
//    psram_frame_buffer_init();
	
//	while(1)
//	{
//		user_display();
//		co_delay_100us(100);
//	}
//	static uint16_t PSRAM_LCD_FRAME_BUFFER_ORIGIN[128*48];
//	for(uint32_t i = 0; i < 128*40; i++)PSRAM_LCD_FRAME_BUFFER_ORIGIN[i]   = swap_endian(0xf800);;//
//	
//		for(uint32_t i = 0; i < 128*40; i++)PSRAM_LCD_FRAME_BUFFER_ORIGIN[i]   = swap_endian(0xf800);//swap_endian(0xf800);
//		gc9107_set_window(0, 128-1,0, 40-1);	 
//		gc9107_display((128*40),PSRAM_LCD_FRAME_BUFFER_ORIGIN,NULL);
//		gc9107_display_wait_transfer_done();
//		
//		for(uint32_t i = 0; i < 128*40; i++)PSRAM_LCD_FRAME_BUFFER_ORIGIN[i]   = swap_endian(0x001f);//swap_endian(0x001f);
//		gc9107_set_window(0, 128-1,40, 40-1);	 
//		gc9107_display((128*40),PSRAM_LCD_FRAME_BUFFER_ORIGIN,NULL);
//		gc9107_display_wait_transfer_done();
//		co_delay_10us(1);
//		for(uint32_t i = 0; i < 128*48; i++)PSRAM_LCD_FRAME_BUFFER_ORIGIN[i]   =  swap_endian(0x07e0);//swap_endian(0x07e0);
//		gc9107_set_window(0, 128-1,80, 48-1);	 
//		gc9107_display((128*48),PSRAM_LCD_FRAME_BUFFER_ORIGIN,NULL);
//		gc9107_display_wait_transfer_done();
//		co_delay_10us(1);
//		for(uint32_t i = 0; i < 128*40; i++)PSRAM_LCD_FRAME_BUFFER_ORIGIN[i]   = 0xffff;//swap_endian(0xffff);
//		gc9107_set_window(0, 128-1,120, 40-1);	 
//		gc9107_display((128*40),PSRAM_LCD_FRAME_BUFFER_ORIGIN,NULL);
//		gc9107_display_wait_transfer_done();
	 
}

void gc9107_set_window(uint16_t x_s, uint16_t x_e, uint16_t y_s, uint16_t y_e)
{
	   
	gc9107_write_cmd(0x2a);//列地址设置
	gc9107_write_16bit_data(x_s);
	gc9107_write_16bit_data(x_e);
	gc9107_write_cmd(0x2b);//行地址设置
	gc9107_write_16bit_data(y_s+0x20);
	gc9107_write_16bit_data(y_e+0x20);
    gc9107_write_cmd(0x2c);//储存器写
}

void gc9107_display_wait_transfer_done(void)
{
    while(dma_transfer_done == false);
}

void gc9107_display(uint32_t pixel_count, uint16_t *data, void (*callback)(void))
{
	#if 1
	  uint32_t i;
    uint32_t total_count = pixel_count / 2;   // accoding source width
    uint8_t link_count = total_count / 4000;
    
    if(dma_transfer_done == false)
    {
        return;
    }
    else
    {
        dma_transfer_done = false;
    }
    
    dma_trans_done_callback = callback;
    
    if(total_count % 4000)
    {
        link_count++;
    }
    
    for (i = 0; i < link_count; i++)
    {
        uint8_t all_set = (total_count <= 4000);
        
        LinkParameter.SrcAddr          = (uint32_t)&data[i * 8000];
        LinkParameter.DstAddr          = (uint32_t)&gc9107_SPI_Handle.SPIx->DR;
        if(all_set)
        {
            LinkParameter.NextLink     = 0;
        }
        else
        {
            LinkParameter.NextLink     = (uint32_t)&Link_Channel[i + 1];
        }
        LinkParameter.Data_Flow        = DMA_M2P_DMAC;
        LinkParameter.Request_ID       = gc9107_DMA_Channel.Init.Request_ID;
        LinkParameter.Source_Inc       = DMA_ADDR_INC_INC;
        LinkParameter.Desination_Inc   = DMA_ADDR_INC_NO_CHANGE;
        LinkParameter.Source_Width     = DMA_TRANSFER_WIDTH_32;
        LinkParameter.Desination_Width = DMA_TRANSFER_WIDTH_8;
        LinkParameter.Burst_Len        = DMA_BURST_LEN_4;
        LinkParameter.Size             = all_set ? (total_count) : 4000;
        LinkParameter.gather_enable    = 0;
        LinkParameter.scatter_enable   = 0;
        total_count -= 4000;

        dma_linked_list_init(&Link_Channel[i], &LinkParameter);
    }
//	
	//__SPI_DATA_FRAME_SIZE(gc9107_SPI_Handle.SPIx,SPI_FRAME_SIZE_8BIT);
	LCD_RELEASE_DC();
    LCD_SET_CS();
    spi_master_transmit_X1_DMA(&gc9107_SPI_Handle);
    dma_linked_list_start_IT(Link_Channel, &LinkParameter, gc9107_DMA_Channel.Channel);
		#endif
 
}

 

__attribute__((section("ram_code"))) void gc9107_dma_isr(void)
{
    void (*callback)();
    while(__SPI_IS_BUSY(gc9107_SPI_Handle.SPIx));
	//	printf("gc9c01_dma_isr\r\n");
    // CS Release
    LCD_RELEASE_CS();
	
    /* Clear Transfer complete status */
    dma_clear_tfr_Status(gc9107_DMA_Channel.Channel);
    /* channel Transfer complete interrupt disable */
    dma_tfr_interrupt_disable(gc9107_DMA_Channel.Channel);

    __SPI_DISABLE(gc9107_SPI_Handle.SPIx);
 //   __SPI_DATA_FRAME_SIZE(gc9107_SPI_Handle.SPIx, SPI_FRAME_SIZE_8BIT);
    
    dma_transfer_done = true;
    callback = dma_trans_done_callback;
    dma_trans_done_callback = NULL;
    if(callback) {
        callback();
    }
}

//__attribute__((section("ram_code"))) void dma_isr(void)
//{
//		gc9107_dma_isr();
//}