#include <stdint.h>
#include "driver_cst816d.h"
#include "lvgl.h"
#include "lv_demos.h"

#include "co_printf.h"
#include "os_timer.h"

#include "driver_system.h"
#include "driver_timer.h"
#include "driver_display.h"
#include "driver_touchpad.h"
#include "driver_st77903.h"
#include "input_driver.h"
#include "driver_ktm57xx.h"
#include "gif_decoder.h"
#include "user_knob_config.h"

#include "driver_pmu.h"
#include "driver_uart.h"
#include "driver_psram.h"
#include "os_mem.h"

#define TOUCHPAD_ENABLED		0
#define LV_TICK_COUNT           5


#ifdef GUI_SLEEP_ENABLED
#define SYSTEM_SLEEP_TIME		30000
#endif

static os_timer_t lv_schedule_timer;
 

static lv_disp_draw_buf_t disp_buf;

static lv_disp_drv_t *last_disp = NULL;
#ifdef DISPLAY_TYPE_ST77903
static bool g_update_flag=0;
static uint16_t *ST77903_PSRAM_BUFF =NULL;
static uint16_t g_update_timeout_cnt=0;
static uint8_t g_gif_show_flag=0;
#endif
uint32_t g_record_last_tick = 0;
uint8_t g_key_code = 0;
uint8_t g_sys_status = 0;//0 run 1 sleep

// this function is called in DMA interrupt
static void my_disp_flush_done(void)
{
//    lv_disp_flush_ready(last_disp);
    last_disp = NULL;
}



static void my_disp_flush(lv_disp_drv_t * disp, const lv_area_t * area, lv_color_t * color_p)
{
 
	#if 1
    if(last_disp != NULL) {
        display_wait_transfer_done();
    }
    last_disp = disp;
#ifdef DISPLAY_TYPE_ST77903
		if(g_gif_show_flag)
		{
			  g_gif_show_flag=0;
		}
		g_update_flag=false;
		g_update_timeout_cnt=0;
		while((get_st77903_send_state()==0));
//	  while((get_st77903_send_state()==0) && (g_update_timeout_cnt<100));
		ST77903_PSRAM_BUFF = (void *)color_p;
		st77903_display(ST77903_PSRAM_BUFF,0);
		g_update_flag=true;
#else
	display_set_window(area->x1, area->x2, area->y1, area->y2);
    display_update((area->x2+1-area->x1)*(area->y2+1-area->y1), (void *)color_p, my_disp_flush_done);

	#endif
    lv_disp_flush_ready(last_disp);
	#endif
 
}

#if(TOUCHPAD_ENABLED==1)
static void my_touchpad_read(struct _lv_indev_drv_t * indev, lv_indev_data_t * data)
{
    uint8_t buffer[8];
		cst816_read_bytes(0x00,buffer,7); 
    //touchpad_read_data_raw(buffer, 8);
    data->state = (buffer[2] != 0) ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
    if(data->state == LV_INDEV_STATE_PR) {
        data->point.x = ((buffer[3]&0x0f)<<8) | buffer[4];
        data->point.y = ((buffer[5]&0x0f)<<8) | buffer[6];
    }	
}
#endif

 
__attribute__((section("ram_code"))) void timer0_isr(void)
{
	
#ifdef DISPLAY_TYPE_ST77903
		g_update_timeout_cnt++;
		if((ST77903_PSRAM_BUFF!=NULL) && get_st77903_send_state() && (g_update_flag == true)) 
		{
					st77903_display(ST77903_PSRAM_BUFF,0);
		}
		else
		{
			if(g_gif_show_flag==1)
			{
				start_gif_decoder_handler();
				//gif_decoder_handler();
			}
		}
#endif
		timer_int_clear(Timer0);
		lv_tick_inc(LV_TICK_COUNT);
}

#ifdef GUI_SLEEP_ENABLED	
//注意：为了减少漏电 需要把用到的外设io进行配置为pmu模式 上拉输入或者输出低电平根据具体硬件电路设计来配置。 
void app_enter_sleep_handler(void)
{
	if(g_group!=NULL)
	{
		lv_group_remove_all_objs(g_group);
	}
	if(prj_parent_cont!=NULL)
	{
		lv_obj_clean(prj_parent_cont);
	}
 
	pmu_ioldosw_ctrl(false);
	os_timer_stop(&lv_schedule_timer);
	stop_encoder_timer();
	timer_stop(Timer0);
	NVIC_DisableIRQ(TIMER0_IRQn);
	wakeup_gpio_cfg();//config wakeup io handler
}
 
void check_sys_sleep_handler(void)
{
	uint32_t elapsed_time = (system_get_curr_time() - g_record_last_tick) & 0x4FFFFFF ;
	if(elapsed_time>SYSTEM_SLEEP_TIME)
	{
		g_record_last_tick = system_get_curr_time();
		g_sys_status = 1;
		app_enter_sleep_handler();
		printf("system_sleep_enable:%d\r\n",os_get_free_heap_size());
		system_sleep_enable();
	}
 
}

void clear_sleep_record_time(void)
{
	g_record_last_tick = system_get_curr_time();
}
//注意：进入睡眠配置为pmu控制的io和外设 唤醒后需要重新初始化 使能给cpu控制。
void app_exit_sleep_handler(void)
{
	system_sleep_disable();
	clear_sleep_record_time();
	pmu_port_wakeup_func_clear(GPIO_PORT_D,(1<<GPIO_BIT_2));
	pmu_port_wakeup_func_clear(GPIO_PORT_D,(1<<GPIO_BIT_3));
	
	pmu_ioldosw_ctrl(true); 
	__SYSTEM_GPIO_CLK_ENABLE();
    system_regs->mdm_qspi_cfg.qspi_ref_128m_en = 1; // configure qspi reference clock to 128MHz
    system_regs->mdm_qspi_cfg.qspi_ref_clk_sel = 1; // qspi is used for internal flash, set its reference clock to 96MHz
    system_set_internal_flash_clock_div(0);
    // configure PSRAM pin and init PSRAM
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_0, PORTC0_FUNC_QSPI0_IO3);
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_1, PORTC1_FUNC_QSPI0_SCLK0);
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_2, PORTC2_FUNC_QSPI0_CSN0);
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_3, PORTC3_FUNC_QSPI0_IO1);
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_4, PORTC4_FUNC_QSPI0_IO2);
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_5, PORTC5_FUNC_QSPI0_IO0);
    psram_init(); 
	
 
 	display_init();
	__SYSTEM_TIMER_CLK_ENABLE();
	timer_init(Timer0, system_get_clock_config()*1000*LV_TICK_COUNT, TIMER_DIV_NONE);
	timer_start(Timer0);
	NVIC_SetPriority(TIMER0_IRQn, 5);
	NVIC_EnableIRQ(TIMER0_IRQn);
	os_timer_start(&lv_schedule_timer, 10, true);
	input_encoder_init();
	prj_prev_cont = lv_obj_create(prj_parent_cont);
	knob_home_page_entry(prj_prev_cont);
	printf("app_exit_sleep_handler %d\r\n",os_get_free_heap_size());
}
#endif


static void lv_schedule_timer_handler(void *arg)
{
 
    lv_timer_handler();
	#ifdef GUI_SLEEP_ENABLED
	check_sys_sleep_handler();
	#endif
}


extern lv_indev_t * g_indev_keypad;
extern void knob_return_home_page(void);

/*Get the currently being pressed key.  0 if no key is pressed*/
static uint32_t keypad_get_key(void)
{
    /*Your code comes here*/
   return g_key_code;
}
/*Get the x and y coordinates if the mouse is pressed*/
static void mouse_get_xy(lv_coord_t * x, lv_coord_t * y)
{
    /*Your code comes here*/

    (*x) = 0;
    (*y) = 0;
}
/*Will be called by the library to read the mouse*/
static void keypad_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    static uint32_t last_key = 0;
    /*Get the current x and y coordinates*/
    mouse_get_xy(&data->point.x, &data->point.y);
    /*Get whether the a key is pressed and save the pressed key*/
    uint32_t act_key = keypad_get_key();
    if(act_key != 0) {
        data->state = LV_INDEV_STATE_PR;
        /*Translate the keys to LVGL control characters according to your key definitions*/
        switch(act_key) {
        case 1:
            act_key = LV_KEY_NEXT;
            break;
        case 2:
            act_key = LV_KEY_PREV;
            break;
        case 3:
            act_key = LV_KEY_LEFT;
            break;
        case 4:
            act_key = LV_KEY_RIGHT;
            break;
        case 5:
            act_key = LV_KEY_ENTER;
            break;
        }
		if(DBLCLICK_CODE == act_key)
		{		 
			 data->state = LV_INDEV_STATE_REL;
			 last_key = act_key;
			 g_key_code = 0;
			 knob_return_home_page();
		   return;
		}
        last_key = act_key;
        g_key_code = 0;
		#ifdef GUI_SLEEP_ENABLED
		clear_sleep_record_time();
		#endif
    //    printf("%d keydown \n",last_key);
    } else {
        data->state = LV_INDEV_STATE_REL;
    }

    data->key = last_key;

    /*Return `false` because we are not buffering and no more data to read*/
    return;
}





void gui_main(void)
{
	#if(TOUCHPAD_ENABLED==1)
	cst816_init();
	#endif
	printf("gui_main\r\n");
	lv_init();
	/*Initialize `disp_buf` with the buffer(s) */
	#if 1 
	lv_disp_draw_buf_init(&disp_buf, (void *)0x22000000, (void *)/*0x22050000*/(0x22000000+LV_HOR_RES_MAX * LV_VER_RES_MAX*2), LV_HOR_RES_MAX * LV_VER_RES_MAX);    /*Initialize the display buffer*/
	//lv_disp_draw_buf_init(&disp_buf, (void *)0x22000000, NULL, LV_HOR_RES_MAX * LV_VER_RES_MAX);    /*Initialize the display buffer*/
	#else
	static lv_color_t buf_1[LV_HOR_RES_MAX * 20];
	static lv_color_t buf_2[LV_HOR_RES_MAX * 20];
	lv_disp_draw_buf_init(&disp_buf, buf_1, buf_2, LV_HOR_RES_MAX * 20);    /*Initialize the display buffer*/
	#endif
	/* Implement and register a function which can copy the rendered image to an area of your display */
	static lv_disp_drv_t disp_drv;               /*Descriptor of a display driver*/
	lv_disp_drv_init(&disp_drv);          /*Basic initialization*/
	disp_drv.flush_cb = my_disp_flush;    /*Set your driver function*/
	disp_drv.draw_buf = &disp_buf;        /*Assign the buffer to the display*/
	disp_drv.hor_res = LV_HOR_RES_MAX;
	disp_drv.ver_res = LV_VER_RES_MAX;
	disp_drv.full_refresh = 1;
	lv_disp_drv_register(&disp_drv);      /*Finally register the driver*/
	#if(TOUCHPAD_ENABLED==1)
	/* Implement and register a function which can read an input device. E.g. for a touch pad */
	static lv_indev_drv_t indev_drv;                  /*Descriptor of a input device driver*/
	lv_indev_drv_init(&indev_drv);             /*Basic initialization*/
	indev_drv.type = LV_INDEV_TYPE_POINTER;    /*Touch pad is a pointer-like device*/
	indev_drv.read_cb = my_touchpad_read;      /*Set your driver function*/
	lv_indev_drv_register(&indev_drv);         /*Finally register the driver*/
	#endif
	input_encoder_init();
	static lv_indev_drv_t indev_drv1;
	/*Initialize your keypad or keyboard if you have*/
	/*Register a keypad input device*/
	lv_indev_drv_init(&indev_drv1);
	indev_drv1.type = LV_INDEV_TYPE_KEYPAD;
	indev_drv1.read_cb = keypad_read;
	g_indev_keypad = lv_indev_drv_register(&indev_drv1);
 	knob_gui_init();

 	display_init();
	__SYSTEM_TIMER_CLK_ENABLE();
	timer_init(Timer0, system_get_clock_config()*1000*LV_TICK_COUNT, TIMER_DIV_NONE);
	timer_start(Timer0);
	NVIC_SetPriority(TIMER0_IRQn, 5);
	NVIC_EnableIRQ(TIMER0_IRQn);
	#ifdef DISPLAY_TYPE_ST77903
	g_gif_show_flag=1;
	start_gif_decoder_init();
	#endif
	os_timer_init(&lv_schedule_timer, lv_schedule_timer_handler, NULL);
	os_timer_start(&lv_schedule_timer, 10, true);
		
    //lv_demo_widgets();//内存占用大 需要把LV_MEM_CUSTOM配置为1才能运行
    //lv_demo_benchmark();
    
     
}
 



